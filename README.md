# Modern Health x Smartling - Gitlab Script

**UPDATE** - SEPT 4th, 2020 - The script now supports multiple repositories. In order for this to work, you are required to use a repo alias that matches your repository name **exactly**. For example, if your repo URL is https://gitlab.com/account-name/repository-name.git, the "alias" key in your `repo-connector.conf` file should be "repository-name". Otherwise, there would be no way of matching the files in your Smartling project with the Gitlab repository it was taken from.

Also, support for environment variables in the `gitlab-api-config.json` file has been added.

## Installation/Running

The script can be run in a docker container by running the following 2 terminal commands:

`docker build -t gitlab-api .`

`docker run -it --rm --name gitlab-script gitlab-api`

It can also be run directly by running the following 2 commands:

`pip install -r requirements.txt`

`python3 gitlab-api-controller.py`

## Configuration

The script needs to be configured before it will run successfully. Configuration is done through editing the values in the supplementary `gitlab-api-config.json` file. The values needed within the config file are described as follows:


1. `gitlabRepoUrl` - Here you'll provide the full URL path to your Gitlab repository. An example of a correct path would be "https://gitlab.com/account-name/repository-name.git"

2. `gitlabAccessToken` - This script uses the Gitlab REST API to function, which requires that a valid access token is supplied with each API request. Here is where you'll provide a Gitlab Access Token. Note that this token will need to possess read+write access to the repository, as the script will be making commits to branches within the repo and cannot do so without those permissions.

3. `gitlabSourceBranchName` - Here you'll provide the name of the branch that will be used as a source of comparison. Each file in your Smartling project will looked at to discern which branch in the repo that file was uploaded from. The script will compare the branch that uploaded the file against the branch specified here as the source of comparison. If the script finds that there is a difference between the English file that was uploaded to Smartling versus the version of the same file on the source branch, only then will it try to commit the translated version(s) of the file back to the branch from where it was uploaded. Note: You will likely want to put the name of your development branch ("develop") here.

4. `smartlingUserId`, `smartlingTokenSecret`, and `smartlingProjectId` - Here you'll provide the API token information received from your Smartling project. You can use the same settings are you are currently using for your Repo Connector integration. These will define which Smartling project the script will watch.

5. `intervalTimer` - Here you'll provide the length of time the script will wait after completing before running once again. The accepted length of time is in minute format.


## Description

This script is intended to supplement an existing Repo Connector integration. The first step that needs to be taken is to **turn off the download process of your current Repo Connector configuration**. This can be done by editing each `smartling-config.json` file you are using so that there is no `translationPathExpression` value included. Without a `translationPathExpression`, the Repo Connector cannot push translations back to the branch its configured to watch, which is exactly what we want -- we'll be using this script to handle that behavior instead.

The Repo Connector will continue to upload files to your Smartling project, so be sure to continue to have it running as normal. Also, be sure to keep translating the files in your project. You will want to make sure the files in the project are up to date with translations more often then not to ensure that feature branches are provided with those translations as swiftly as possible.

Run the script to immediately have it begin scanning the files that have been uploaded to your Smartling project. (Note that it will scan **all** of the files in your project, so if you have any old files that you don't wish to include in this process, be sure to delete them before running the script) One-by-one, starting with the most recently uploaded file in the project, the script will look at the files and perform the following actions:


1. First it will find out the name of the branch the file was uploaded from. Using the branch name, it will compare the version of the file that was uploaded against the version of the file that exists (if any) on the branch provided in the `gitlabSourceBranchName` value of the `gitlab-api-config.json` file. If it detects that there has been any change in the file, or if it detects that this is a new file that doesn't exist on the source branch, the script will earmark this file as one that needs to have its translations committed back to the same branch from which it came.

Alternatively, if the script finds no differences between the file on the source branch versus the file on the branch it was uploaded from, it is assumed that there will not be any need to commit translations for this file and the file will be skipped.

2. The script will check the status of translations for this particular file. If it finds that translations have completed in one or more languages for this file, the script will take note of those languages.

3. The script will then download the fully translated files to the local drive of the computer running the script.

4. The script will then commit each of these translations, one-by-one, directly back to the branch the English file was uploaded from. If it finds that there is an existing version of the translated file on the branch, it will attempt to "update" it. If there is nothing to update (meaning there have been no changes to the translated file since that last time it was committed), no commit will be made. (Note: a folder with the translation locale name will be created on each branch where the script makes its commits. The folder will contain the translations that were committed, with the directory structure preserved within)

5. The script is designed to be run continuously. It will loop through each file in the Smartling project, wait for the given period of time designated in the `gitlab-api-config.json` file, then run again. Feel free to stop it at any time. It will kickoff the entire file-scanning process all over again whenever it is run.
