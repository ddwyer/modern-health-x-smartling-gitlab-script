#! python3
# Modern Health Smartling-GitLab Repository Integration
#
# Downloads completed translations from Smartling and sends them to
# GitLab branches in accordance to custom configurations.
# Created by Donovan Dwyer (ddwyer@smartling.com) for customer Modern Health

import requests
import re
import base64
import json
import os
import shutil
import time
from datetime import datetime
from pathlib import Path

##################################################
#### --- GITLAB REPO MANAGEMENT FUNCTIONS --- ####
##################################################

class config():
    # Formatting config file
    config_data = open('./gitlab-api-config.json').read()
    config_data = re.sub("\/\/ .+", "", config_data)
    config_data = re.sub("/", "\/", config_data)
    environment_variables = re.findall(r'\${\w+}', config_data)
    for variable in environment_variables:
        env_variable = re.sub("\$", "", variable)
        env_variable = re.sub("{|}","", env_variable)
        config_data = re.sub("\${%s}" %(env_variable), '"%s"' %(os.environ[env_variable]), config_data)
    config_data = json.loads(config_data)

    # Setting general config options
    gitlab_base_url = 'https://gitlab.com/api/v4'
    gitlab_repo_data = {}
    smartling_base_url = 'https://api.smartling.com'
    smartling_user_id = config_data['smartlingUserId']
    smartling_token_secret = config_data['smartlingTokenSecret']
    smartling_project_id = config_data['smartlingProjectId']
    interval_timer = config_data['intervalTimer']
    olderThan = config_data['ignoreFilesOlderThan']
    # Setting repo config options
    for repo in config_data['repositories']:
        repo_url = repo['gitlabRepoUrl'].split('/')
        repo_name = repo_url[-1].split('.')
        seperator = '%2F'
        formatted_path = seperator.join([repo_url[-2], repo_name[0]])
        url = gitlab_base_url + '/projects/%s' %(formatted_path)
        r = requests.get(url, headers={'PRIVATE-TOKEN': repo['gitlabAccessToken']})
        gitlab_repo_data[repo_name[0]] = {
            'id' : r.json()['id'],
            'token' : {'PRIVATE-TOKEN': repo['gitlabAccessToken']},
            'branch' : repo['gitlabSourceBranchName']
        }

def new_branch(branch_name, repo_obj, ref_name):
    "Creates branch using given branch name, repo ID, and reference branch name"
    url = config.gitlab_base_url + '/projects/%s/repository/branches' %(repo_obj['id'])
    params = {'branch': branch_name, 'ref': ref_name}
    r = requests.post(url, headers=repo_obj['token'], params=params)
    return r.json()

def delete_branch(repo_obj, branch_name):
    "Deletes a given branch"
    url = config.gitlab_base_url + '/projects/%s/repository/branches/%s' %(repo_obj['id'], branch_name)
    r = requests.delete(url, headers=repo_obj['token'])

def get_branch(repo_obj, branch_name):
    "Fetches a single branch by name"
    url = config.gitlab_base_url + '/projects/%s/repository/branches/%s' %(repo_obj['id'], branch_name)
    r = requests.get(url, headers=repo_obj['token'])
    return r.json()

def list_branches(repo_obj, **kwargs):
    "Gets list of branches associated with a repo id w/ optional search regex"
    search = kwargs.get('search', None)
    params = {'search': search}
    url = config.gitlab_base_url + '/projects/%s/repository/branches' %(repo_obj['id'])
    r = requests.get(url, headers=repo_obj['token'], params=params)
    return r.json()

def create_commit(repo_obj, branch_name, locale, file_uri):
    "Commits file to a branch"
    if fetch_from_repo(repo_obj, get_abs_file_path_from_uri(file_uri), branch_name) == '404':
        action = 'create'
    else:
        if fetch_from_repo(repo_obj, get_abs_file_path_from_uri(file_uri), branch_name) ['size'] == Path(file_uri).stat().st_size:
            log_capture("File already exists on branch. No commit made.")
            return 'break'
        else:
            log_capture("File already exists on branch. Commit will updated existing file.")
            action = 'update'
    file = open(file_uri).read()
    payload = {
        'branch': branch_name,
        'commit_message': 'feat: translation strings from en-US to %s' %(locale),
        'actions': [
            {
                'action': action,
                'file_path': get_abs_file_path_from_uri(file_uri),
                'content': file
            }
        ]
    }
    url = config.gitlab_base_url + '/projects/%s/repository/commits' %(repo_obj['id'])
    r = requests.post(url, headers=repo_obj['token'], json=payload)
    log_capture("File '%s' committed to branch %s." %(file_uri, branch_name))
    return r.json()

def create_merge_request(repo_obj, target_branch, title):
    "Creates a merge request to specified branch"
    params = {
        'source_branch': repo_obj['branch'],
        'target_branch': target_branch,
        'title': title
    }
    url = config.gitlab_base_url + '/projects/%s/merge_requests' %(repo_ob['id'])
    r = requests.post(url, headers=repo_obj['token'], params=params)
    return r.json()

def fetch_from_repo(repo_obj, file_path, branch_name):
    "Downloads file from repository"
    path = re.sub("\/", "%2F", file_path)
    path = re.sub("\.", "%2E", path)
    params = {'ref': branch_name}
    url = config.gitlab_base_url + '/projects/%s/repository/files/%s?' %(repo_obj['id'], path)
    r = requests.get(url, headers=repo_obj['token'], params=params)
    parsed_res = r.json()
    if parsed_res == {'message' : '404 File Not Found'}:
        return '404'
    else:
        return {
            'content': base64.decodebytes(parsed_res['content'].encode('ASCII')),
            'size': parsed_res['size']
        }

def read_smartling_config(repo_obj, branch_name):
    "Returns the Smartling Config JSON file data from a repo branch"
    res = fetch_from_repo(repo_obj, 'smartling-config.json', branch_name)['content']
    parsed_res = res.decode('utf-8')
    parsed = re.sub("\/\/[\w\d\s\\\.:\"'()/,+#\-]+\\n", "", parsed_res)
    return json.loads(parsed)

def read_json_file(path):
    "Open JSON file"
    file = open(path).read()
    return json.loads(file)

def write_file(content, file_path):
    "Writes a file when given data and file path"
    with open(file_path, "wb") as file:
        file.write(content)

def compare_branches(repo_obj, target_branch):
    "Compares two branches and returns the diffs, if any exist"
    params = {
        'from': repo_obj['branch'],
        'to': target_branch
    }
    url = config.gitlab_base_url + '/projects/%s/repository/compare' %(repo_obj['id'])
    r = requests.get(url, headers=repo_obj['token'], params=params)
    parsed_r = r.json()
    return parsed_r['diffs']

def get_repo_obj(file_uri):
    return config.gitlab_repo_data[get_repo_name_from_uri(file['fileUri'])]

def repo_and_branch_exist(repo_name, branch):
    "Checks for existence of repository when given a name"
    if repo_name in config.gitlab_repo_data.keys() and get_branch(config.gitlab_repo_data[repo_name], branch) != {'message': '404 Branch Not Found'}:
        return True
    else:
        return False


################################################
#### --- SMARTLING MANAGEMENT FUNCTIONS --- ####
################################################

def generate_smartling_headers():
    "Generates Smartling access token"
    json_payload = {'userIdentifier': config.smartling_user_id, 'userSecret': config.smartling_token_secret}
    url = config.smartling_base_url + '/auth-api/v2/authenticate'
    r = requests.post(url, json=json_payload)
    parsed_req = r.json()
    if parsed_req['response']['code'] == 'SUCCESS':
        return {'Authorization': 'Bearer %s' %(parsed_req['response']['data']['accessToken'])}

def get_list_of_files(project_id):
    "Fetches list of files uploaded to specified Smartling project"
    url = config.smartling_base_url + '/files-api/v2/projects/%s/files/list' %(project_id)
    r = requests.get(url, headers=generate_smartling_headers())
    parsed_req = r.json()
    if parsed_req['response']['data']['totalCount'] <= 100:
        return parsed_req['response']['data']['items']
    else:
        files = parsed_req['response']['data']['items']
        total = parsed_req['response']['data']['totalCount'] - 100
        offset = 100
        while (total > 0):
            params = {
                'offset': offset,
                'orderBy': 'lastUploaded_desc'
            }
            r = requests.get(url, headers=generate_smartling_headers(), params=params)
            files.extend(r.json()['response']['data']['items'])
            total = total - 100
            offset = offset + 100
        return files

def get_translation_status_for_file(project_id, file_uri):
    "Gets status of translation for each locale in a file"
    params = { 'fileUri': file_uri }
    url = config.smartling_base_url + '/files-api/v2/projects/%s/file/status' %(project_id)
    r = requests.get(url, headers=generate_smartling_headers(), params=params)
    return r.json()

def download_translations(project_id, file_uri, locale, retrieval_type='published', include_org_strs=True):
    "Downloads translated file given specifications and then commits it to repo"
    params = {
        'fileUri': file_uri,
        'retrievalType': retrieval_type,
        'includeOriginalStrings': include_org_strs
    }
    url = config.smartling_base_url + '/files-api/v2/projects/%s/locales/%s/file' %(project_id, locale)
    r = requests.get(url, headers=generate_smartling_headers(), params=params, stream=True)
    file_path = dir_formatter(file_uri, locale)
    log_capture("Dowloading %s translation for file '%s' to local environment" %(locale, file_uri))
    with open(file_path, 'wb') as translated_file:
        for chunk in r.iter_content(chunk_size=128):
            translated_file.write(chunk)
    log_capture("Trying to commit file %s in %s language to branch %s" %(get_filename_from_uri(file_uri), locale, get_branch_name_from_uri(file_uri)))
    create_commit(config.gitlab_repo_data[get_repo_name_from_uri(file_uri)], get_branch_name_from_uri(file_uri), locale, file_path)

def dir_formatter(file_path, locale, locale_dir=True):
    "Handles the path formatting to preserve dir structure"
    repo = get_repo_name_from_uri(file_path)
    branch = get_branch_name_from_uri(file_path)
    path = get_dir_from_uri(file_path)
    if "en-US" in path:
        path = re.sub("en-US", locale, path)
    else:
        path = "%s/%s" %(locale, path)
    filename = get_filename_from_uri(file_path)
    if locale_dir:
        full_path = "%s/%s/%s/%s" %(repo, branch, path, filename)
        dir_path = "%s/%s/%s" %(repo, branch, path)
    else:
        file_ext = filename.split('.')[-1]
        file = filename.split('.')[0]
        full_path = "%s/%s/%s/%s-%s.%s" %(repo, branch, path, file, locale, file_ext)
        dir_path = "%s/%s/%s/" %(repo, branch, path)
    if os.path.isdir(dir_path):
        shutil.rmtree(dir_path)
    os.makedirs(dir_path)
    return full_path

def get_branch_name_from_uri(file_uri):
    return file_uri.split('/')[2]

def get_repo_name_from_uri(file_uri):
    return file_uri.split('/')[1]

def get_abs_file_path_from_uri(file_uri):
    file_path = file_uri.split('/')
    seperator = '/'
    return seperator.join(file_path[2:])

def get_filename_from_uri(file_uri):
    file_path = file_uri.split('/')
    seperator = '/'
    return seperator.join(file_path[-1:])

def get_dir_from_uri(file_uri):
    file_path = file_uri.split('/')
    seperator = '/'
    return seperator.join(file_path[3:-1])

def compare_dates(date):
    "Returns the amount of time elapsed now and a given date"
    now = datetime.now().date()
    parsed_date = date.split("T")[0]
    parsed_date = datetime.strptime(parsed_date,'%Y-%m-%d').date()
    delta = now - parsed_date
    return delta.days

def log_capture(string, verbose=True):
    "Prints script actions to console and logs them in log file"
    now = datetime.now()
    current_time = now.strftime("%d/%m/%Y %H:%M:%S")
    if verbose:
        print("[%s] - %s" %(current_time, string))

def main():
    while True:
        fileUri_queue = []
        whitelist = []
        blacklist = []
        manifest = {}
        log_capture("Starting round trip...")
        log_capture("Fetching files from Smartling project...")
        request = get_list_of_files(config.smartling_project_id)
        if (len(request) > 0):
            for file in request:
                if file['fileUri'] not in fileUri_queue and len(file['fileUri'].split('/')) > 2:
                    if compare_dates(file['lastUploaded']) > config.olderThan:
                        log_capture("File older than 30 days. Skipping file: '%s'" %(file['fileUri']))
                    else:
                        fileUri_queue.append(file['fileUri'])
                        log_capture("Adding file to queue for status check: '%s'" %(file['fileUri']))
                if len(file['fileUri'].split('/')) > 2:
                    branch_name = get_branch_name_from_uri(file['fileUri'])
                    if branch_name not in manifest and repo_and_branch_exist(get_repo_name_from_uri(file['fileUri']), branch_name) and compare_dates(file['lastUploaded']) < config.olderThan:
                        log_capture("Branch and repo confirmed existing for file: '%s'" %(file['fileUri']))
                        locales = []
                        for locale in read_smartling_config(config.gitlab_repo_data[get_repo_name_from_uri(file['fileUri'])], branch_name)['locales']:
                            locales.append(locale['smartling'])
                        log_capture("Loading list of target locales for file '%s'" %(file['fileUri']))
                        manifest[branch_name] = {
                            'locales': locales,
                            'source_repo_obj': config.gitlab_repo_data[get_repo_name_from_uri(file['fileUri'])],
                            'source_repo_name': get_repo_name_from_uri(file['fileUri'])
                        }
                    elif repo_and_branch_exist(get_repo_name_from_uri(file['fileUri']), branch_name) == False:
                        log_capture("No existing branch/repo found for file '%s'" %(file['fileUri']))
            for branch in manifest.keys():
                if branch not in blacklist and repo_and_branch_exist(manifest[branch]['source_repo_name'], branch):
                    log_capture("Comparing files in branch '%s' against source branch '%s'" %(branch, manifest[branch]['source_repo_obj']['branch']))
                    diffs = compare_branches(config.gitlab_repo_data[manifest[branch]['source_repo_name']], branch)
                    if len(diffs) > 0:
                        log_capture(str(len(diffs)) + " diff(s) found")
                        for diff in diffs:
                            if ('/%s/%s/%s' %(manifest[branch]['source_repo_name'], branch, diff['new_path'])) in fileUri_queue:
                                log_capture("Adding file at path %s to queue for committing" %(diff['new_path']))
                                whitelist.append('/%s/%s/%s' %(manifest[branch]['source_repo_name'], branch, diff['new_path']))
                    else:
                        log_capture("No diffs found for branch %s" %(branch))
                        blacklist.append(branch)
            for file_uri in whitelist:
                if file_uri in fileUri_queue and repo_and_branch_exist(get_repo_name_from_uri(file_uri), get_branch_name_from_uri(file_uri)):
                    log_capture("Checking translation status for file: %s" %(file_uri))
                    translation_status = get_translation_status_for_file(config.smartling_project_id, file_uri)
                    file_string_count = translation_status['response']['data']['totalStringCount']
                    locale_data = translation_status['response']['data']['items']
                    for locale in locale_data:
                        if locale['completedStringCount'] == file_string_count and locale['localeId'] in manifest[get_branch_name_from_uri(file_uri)]['locales']:
                            log_capture("%s translation done for file %s" %(locale['localeId'], file_uri))
                            download_translations(config.smartling_project_id, file_uri, locale['localeId'])
                else:
                    log_capture("File %s skipped" %(file_uri))
                    whitelist.remove(file_uri)
        else:
            log_capture("No relevant files exist in Smartling project.")
        log_capture("Completed full round trip.")
        timer = config.interval_timer
        while timer > 0:
            log_capture("Sleeping for %s minutes..." %(timer))
            timer = timer - 1
            time.sleep(60)

if __name__ == "__main__":
    main()
